﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesTracker
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class StatisticsWindow : Window
    {
        public List<DateStatistics> asdf { get; set; }
        public MainWindow mainWindow { get; set; }
        public StatisticsWindow()
        {
            InitializeComponent();

        }

        private void Window_Activated(object sender, EventArgs e)
        {
            GraphUpdate.updateGraph(graphCanvas, asdf);

            if (SalesRegression.IsChecked == true)
            {
                GraphUpdate.plotReg();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void checkBox_Checked(object sender, RoutedEventArgs e)
        {
            GraphUpdate.plotReg();
        }

        private void checkBox_Unchecked(object sender, RoutedEventArgs e)
        {
            GraphUpdate.removeReg();
        }
    }
}
