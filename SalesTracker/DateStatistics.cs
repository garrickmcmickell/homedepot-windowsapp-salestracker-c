﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesTracker
{
    public class DateStatistics
    {
        public DateTime date { get; set; }
        public double totalSales { get; set; }
    }
}
