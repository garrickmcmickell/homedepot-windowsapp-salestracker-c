﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SalesTracker
{
    static class GraphUpdate
    {
        public static MainWindow mainWindow { get; set; }
        private static List<Ellipse> canvasCircles = new List<Ellipse>();
        private static List<Line> canvasLines = new List<Line>();
        private static List<Point> canvasPoints = new List<Point>();
        private static List<DateStatistics> dateStatsList;
        private static Line salesRegressionLine;
        private static Canvas currentCanvas;

        public static void updateGraph(Canvas passedCanvas, List<DateStatistics> passedList)
        {
            currentCanvas = passedCanvas;
            dateStatsList = passedList;
            resetCanvas();
            plotPoints();

            if (canvasPoints.Count > 1)
            {
                plotLines();
            }
            
        }

        private static void plotPoints()
        {
            Ellipse point;

            

            double highestSale = dateStatsList[0].totalSales;

            foreach (DateStatistics d in dateStatsList)
            {
                if (d.totalSales > highestSale)
                    highestSale = d.totalSales;
            }

            for (int i = 0; i < dateStatsList.Count; i++)
            {
                point = new Ellipse();
                canvasPoints.Add(new Point((i * ((currentCanvas.Width) / dateStatsList.Count)) + (((currentCanvas.Width) / dateStatsList.Count) / 2) + 1.25, currentCanvas.Height - ((dateStatsList[i].totalSales / highestSale) * currentCanvas.Height) + 3.5));
                canvasCircles.Add(point);
                point.Width = 3;
                point.Height = 3;
                point.Stroke = new SolidColorBrush(Colors.AliceBlue);
                point.StrokeThickness = 1;
                currentCanvas.Children.Add(point);
                Canvas.SetLeft(point, (i * ((currentCanvas.Width) / dateStatsList.Count)) + (((currentCanvas.Width) / dateStatsList.Count) / 2));
                Canvas.SetBottom(point, ((dateStatsList[i].totalSales / highestSale) * currentCanvas.Height) - 5);
            }
        }

        private static void plotLines()
        {
            Line line;

            for (int i = 0; i < canvasPoints.Count - 1; i++)
            {
                line = new Line();
                canvasLines.Add(line);
                currentCanvas.Children.Add(line);
                line.Stroke = new SolidColorBrush(Colors.AliceBlue);
                line.StrokeThickness = 1;

                line.X1 = canvasPoints[i].X;
                line.Y1 = canvasPoints[i].Y;
                line.X2 = canvasPoints[i + 1].X;
                line.Y2 = canvasPoints[i + 1].Y;
            }

        }

        private static void resetCanvas()
        {
            canvasPoints = new List<Point>();

            currentCanvas.Children.Remove(salesRegressionLine);

            foreach (Ellipse e in canvasCircles)
            {
                currentCanvas.Children.Remove(e);
            }

            foreach(Line l in canvasLines)
            {
                currentCanvas.Children.Remove(l);
            }
        }

        public static void plotReg()
        {

            double yInt;
            double slope;
            double dd;

            LinearRegression(out dd, out yInt, out slope);

            double highestSale = dateStatsList[0].totalSales;

            foreach (DateStatistics d in dateStatsList)
            {
                if (d.totalSales > highestSale)
                    highestSale = d.totalSales;
            }

            double adjYInt = (yInt / highestSale) * currentCanvas.Height;
            double adjSlope = (slope / highestSale) * currentCanvas.Height;

            double xStart = (((currentCanvas.Width) / dateStatsList.Count) / 2);
            double yStart = adjYInt;
            double xEnd = ((dateStatsList.Count - 1) * ((currentCanvas.Width) / dateStatsList.Count)) + (((currentCanvas.Width) / dateStatsList.Count) / 2);
            double yEnd = adjSlope * (dateStatsList.Count - 1) + adjYInt;

            Point start = new Point(xStart,yStart);
            Point end = new Point(xEnd,yEnd);

            salesRegressionLine = new Line();
            currentCanvas.Children.Add(salesRegressionLine);
            salesRegressionLine.Stroke = new SolidColorBrush(Colors.Red);
            salesRegressionLine.X1 = xStart;
            salesRegressionLine.Y1 = (currentCanvas.Height - yStart) + 3;
            salesRegressionLine.X2 = xEnd;
            salesRegressionLine.Y2 = (currentCanvas.Height - yEnd) + 3;
            
        }

        public static void removeReg()
        {
            currentCanvas.Children.Remove(salesRegressionLine);
        }

        public static void LinearRegression(out double rsquared, out double yintercept,
                                            out double slope)
        {
            double sumOfX = 0;
            double sumOfY = 0;
            double sumOfXSq = 0;
            double sumOfYSq = 0;
            double ssX = 0;
            double ssY = 0;
            double sumCodeviates = 0;
            double sCo = 0;
            double count = dateStatsList.Count;

            for (int i = 0; i < count; i++)
            {
                double x = i;
                double y = dateStatsList[i].totalSales;
                sumCodeviates += x * y;
                sumOfX += x;
                sumOfY += y;
                sumOfXSq += x * x;
                sumOfYSq += y * y;
            }
            ssX = sumOfXSq - ((sumOfX * sumOfX) / count);
            ssY = sumOfYSq - ((sumOfY * sumOfY) / count);
            double RNumerator = (count * sumCodeviates) - (sumOfX * sumOfY);
            double RDenom = (count * sumOfXSq - (sumOfX * sumOfX))
             * (count * sumOfYSq - (sumOfY * sumOfY));
            sCo = sumCodeviates - ((sumOfX * sumOfY) / count);

            double meanX = sumOfX / count;
            double meanY = sumOfY / count;
            double dblR = RNumerator / Math.Sqrt(RDenom);
            rsquared = dblR * dblR;
            yintercept = meanY - ((sCo / ssX) * meanX);
            slope = sCo / ssX;
        }
    }
}
