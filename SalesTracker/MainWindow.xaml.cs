﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SalesTracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<DateStatistics> currentDateRange = new List<DateStatistics>();
        Random r = new Random();
        StatisticsWindow statsWindow = new StatisticsWindow();
        

        public MainWindow()
        {
            InitializeComponent();
            statsWindow.asdf = currentDateRange;
            statsWindow.mainWindow = this;
            GraphUpdate.mainWindow = this;
            
            for(int i = 0; i < 7; i++)
            {
                DateStatistics d = new DateStatistics();
                d.date = DateTime.Now.AddDays(i);
                d.totalSales = r.Next(0, 15000);
                currentDateRange.Add(d);
            }

            GraphUpdate.updateGraph(graphCanvas, currentDateRange);

            
            
        }



        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            Calendar c = (Calendar)sender;
            SelectedDatesCollection dates = c.SelectedDates;
            
            resetDateStatistics();

            foreach (DateTime t in dates)
            {
                DateStatistics d = new DateStatistics();
                d.date = t;
                d.totalSales = r.Next(0, 15000);
                currentDateRange.Add(d);
            }
            GraphUpdate.updateGraph(graphCanvas, currentDateRange);

            statsWindow.asdf = currentDateRange;
        }

        private void resetDateStatistics()
        {
            currentDateRange = new List<DateStatistics>();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
           
            statsWindow.Show();

        }
    }
}
